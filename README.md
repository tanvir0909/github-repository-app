<h1>Project title: Github Repository App</h1><br>
Language : Java <br>
Software architecture pattern : MVVM <br>
Dependency injection : Dagger <br><br><br>

<h2>App screenshots : </h2><br>
![List](https://gitlab.com/tanvir0909/github-repository-app/-/raw/main/List.PNG)
![Details](https://gitlab.com/tanvir0909/github-repository-app/-/raw/main/Details.PNG)


<h2>Project architecture :</h2><br>
![mvvm](https://s3.ap-south-1.amazonaws.com/mindorks-server-uploads/mvvm.png)
