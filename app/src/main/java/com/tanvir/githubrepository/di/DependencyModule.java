package com.tanvir.githubrepository.di;

import com.tanvir.githubrepository.api.Api;
import com.tanvir.githubrepository.api.NetworkService;

import dagger.Module;
import dagger.Provides;
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DependencyModule {

    private static final String BASE_URL = "https://api.github.com/";

    @Provides
    public Api provideApi(){
        return new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()
            .create(Api.class);
    }

    @Provides
    public NetworkService provideNetworkService(){
         return NetworkService.getInstance();
    }
}
