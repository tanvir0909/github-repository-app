package com.tanvir.githubrepository.di;

import com.tanvir.githubrepository.api.NetworkService;
import com.tanvir.githubrepository.viewModel.ViewModel;

import dagger.Component;

@Component(modules = {DependencyModule.class})
public interface DependencyComponent {

    void inject(NetworkService service);
    void inject(ViewModel viewModel);
}
