package com.tanvir.githubrepository.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import com.tanvir.githubrepository.R;
import com.tanvir.githubrepository.databinding.ActivityMainBinding;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding mainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        // try block to hide Action bar
        try {
            Objects.requireNonNull(this.getSupportActionBar()).hide();
        }
        // catch block to handle NullPointerException
        catch (NullPointerException e) {
        }
    }
}