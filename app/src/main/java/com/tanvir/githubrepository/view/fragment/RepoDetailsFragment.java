package com.tanvir.githubrepository.view.fragment;

import static com.tanvir.githubrepository.utility.Constant.convertDateTime;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tanvir.githubrepository.R;
import com.tanvir.githubrepository.databinding.FragmentRepoDetailsBinding;
import com.tanvir.githubrepository.model.repoDetails.RepoDetailsResponse;
import com.tanvir.githubrepository.model.userDetails.UserDetailsResponse;
import com.tanvir.githubrepository.sqlite.DatabaseHandler;
import com.tanvir.githubrepository.viewModel.ViewModel;

public class RepoDetailsFragment extends Fragment {

    FragmentRepoDetailsBinding detailsBinding;
    private ViewModel viewModel;
    private String repoUrl = "";
    private String userUrl = "";
    private String repoId = "";
    private String userId = "";
    private RequestOptions options;
    String repoDetails = "";
    String userDetails = "";
    DatabaseHandler db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        detailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_repo_details, container, false);
        viewModel = ViewModelProviders.of(this).get(ViewModel.class);
        db = new DatabaseHandler(getContext());

        Bundle arguments = getArguments();
        if (arguments != null) {
            repoUrl = arguments.getString("repoUrl");
        }

        String[] urlArray = repoUrl.split("repos/");
        repoId = urlArray[1];
        urlArray = repoId.split("/");
        userId = urlArray[0];

        options = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background);

        repoDetails = db.getRepoDetails(repoId);
        if (repoDetails.length() > 0) {
            RepoDetailsResponse repoDetailsResponse = new Gson().fromJson(repoDetails, RepoDetailsResponse.class);
            detailsBinding.tvDescription.setText(repoDetailsResponse.getDescription());
            detailsBinding.dateTime.setText(convertDateTime(repoDetailsResponse.getUpdatedAt()));
        }
        userDetails = db.getUserDetails(userId);
        if (userDetails.length() > 0) {
            UserDetailsResponse userDetailsResponse = new Gson().fromJson(userDetails, UserDetailsResponse.class);
            detailsBinding.tvName.setText(userDetailsResponse.getName());
            Glide.with(this).load(userDetailsResponse.getAvatarUrl()).apply(options).into(detailsBinding.profilePhoto);
        }

        detailsBinding.loading.setVisibility(View.VISIBLE);
        viewModel.getRepoDetailsResponse(urlArray[0], urlArray[1]);
        observerRepoDetailsViewModel();

        return detailsBinding.getRoot();
    }

    private void observerRepoDetailsViewModel() {
        viewModel.repoDetailsResponse.observe(
                getViewLifecycleOwner(),
                response -> {
                    String jsonSender = new Gson().toJson(response, new TypeToken<Object>() {
                    }.getType());
                    if (repoDetails.length() > 0) {
                        db.updateRepoDetails(repoId, jsonSender);
                    } else {
                        db.addRepoDetails(repoId, jsonSender);
                    }

                    String[] urlArray = response.getOwner().getUrl().split("users/");
                    userId = urlArray[1];
                    viewModel.getUserDetailsResponse(userId);
                    observerUserDetailsViewModel(response);

                    viewModel.repoDetailsResponse = new MutableLiveData<>();
                }
        );
        viewModel.repoDetailsResponseLoadError.observe(
                getViewLifecycleOwner(), isError -> {
                    if (isError != null) {
                        if (isError) {
                            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                        viewModel.repoDetailsResponseLoadError = new MutableLiveData<>();
                    }
                }
        );

        viewModel.noNetwork.observe(
                getViewLifecycleOwner(), isError -> {
                    if (isError != null) {
                        if (isError) {
                            Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                        }
                        viewModel.noNetwork = new MutableLiveData<>();
                    }
                }
        );

        viewModel.loading.observe(
                getViewLifecycleOwner(), loading -> {
                    if (loading != null) {
                        if (!loading) {
                            detailsBinding.loading.setVisibility(View.GONE);
                        }
                        viewModel.loading = new MutableLiveData<>();
                    }
                }
        );
    }

    private void observerUserDetailsViewModel(RepoDetailsResponse repoResponse) {
        viewModel.userDetailsResponse.observe(
                getViewLifecycleOwner(),
                response -> {
                    String jsonSender = new Gson().toJson(response, new TypeToken<Object>() {
                    }.getType());
                    if (userDetails.length() > 0) {
                        db.updateUserDetails(userId, jsonSender);
                    } else {
                        db.addUserDetails(userId, jsonSender);
                    }

                    detailsBinding.tvName.setText(response.getName());
                    detailsBinding.tvDescription.setText(repoResponse.getDescription());
                    detailsBinding.dateTime.setText(convertDateTime(repoResponse.getUpdatedAt()));
                    Glide.with(this).load(response.getAvatarUrl()).apply(options).into(detailsBinding.profilePhoto);
                    viewModel.userDetailsResponse = new MutableLiveData<>();
                }
        );
        viewModel.userDetailsResponseLoadError.observe(
                getViewLifecycleOwner(), isError -> {
                    if (isError != null) {
                        if (isError) {
                            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                        viewModel.userDetailsResponseLoadError = new MutableLiveData<>();
                    }
                }
        );

        viewModel.noNetwork.observe(
                getViewLifecycleOwner(), isError -> {
                    if (isError != null) {
                        if (isError) {
                            Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                        }
                        viewModel.noNetwork = new MutableLiveData<>();
                    }
                }
        );

        viewModel.loading.observe(
                getViewLifecycleOwner(), loading -> {
                    if (loading != null) {
                        if (!loading) {
                            detailsBinding.loading.setVisibility(View.GONE);
                        }
                        viewModel.loading = new MutableLiveData<>();
                    }
                }
        );

    }

}