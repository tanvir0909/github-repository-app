package com.tanvir.githubrepository.view.fragment;

import static android.content.Context.MODE_PRIVATE;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tanvir.githubrepository.R;
import com.tanvir.githubrepository.adapter.ShowRepositoryListAdapter;
import com.tanvir.githubrepository.databinding.FragmentHomeBinding;
import com.tanvir.githubrepository.model.allRepositories.Item;
import com.tanvir.githubrepository.sqlite.DatabaseHandler;
import com.tanvir.githubrepository.viewModel.ViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class HomeFragment extends Fragment {
    FragmentHomeBinding homeBinding;
    private ViewModel viewModel;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private ShowRepositoryListAdapter repositoryListAdapter;
    private int page, perPage;
    private int currentItems, totalItems, scrollOutItems;
    private boolean isScrolling = false;
    private String sortString = "";
    private Handler handler;
    private Runnable runnable;
    DatabaseHandler db;
    private List<Item> allRepoList;
    private boolean isOffline;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        homeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        viewModel = ViewModelProviders.of(this).get(ViewModel.class);
        db = new DatabaseHandler(getContext());
        allRepoList = new ArrayList<>();
        isOffline = true;

        preferences = getContext().getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        editor = preferences.edit();

        sortString = preferences.getString("sortBy", "stars");

        repositoryListAdapter = new ShowRepositoryListAdapter(new ArrayList<>(), getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        homeBinding.repositoryList.setLayoutManager(layoutManager);
        homeBinding.repositoryList.setAdapter(repositoryListAdapter);

        if (isOffline) {
            String jsonRepo = db.getAllRepository();
            Item[] repo = new Gson().fromJson(jsonRepo, Item[].class);
            if (repo != null){
                repositoryListAdapter.updateRepositoryList(Arrays.asList(repo));
                repositoryListAdapter.notifyDataSetChanged();
            }
        }

        page = 1;
        perPage = 10;

        homeBinding.loading.setVisibility(View.VISIBLE);
        viewModel.getAllRepositoriesResponse("android", page, perPage, sortString);
        observerRepositoryListViewModel();

        homeBinding.btnSort.setOnClickListener(l -> {
            showSortDialog();
        });

        homeBinding.repositoryList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    loadNewRepository();
                }
            }
        });

        repositoryListAdapter.setOnClickItem(url -> {
            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
            Bundle bundle = new Bundle();
            bundle.putString("repoUrl", url);
            navController.navigate(R.id.navigation_repo_details, bundle);
        });


        //Refresh list after every 30 minutes
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                page = 1;
                viewModel.getAllRepositoriesResponse("android", page, perPage, sortString);
                observerRepositoryListViewModel();
                handler.postDelayed(this, 1800000);
            }
        };

        return homeBinding.getRoot();
    }

    private void showSortDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_filter_list, null);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();

        Button cancelButton = view.findViewById(R.id.btn_cancel);
        RadioButton starCount = view.findViewById(R.id.starCount);
        RadioButton updatedDate = view.findViewById(R.id.updatedDateTime);

        if (sortString.equalsIgnoreCase("stars"))
            starCount.setChecked(true);
        else
            updatedDate.setChecked(true);

        starCount.setOnClickListener(l -> {
            if (!sortString.equalsIgnoreCase("stars")) {
                sortString = "stars";
                editor.putString("sortBy", "stars");
                editor.commit();
                page = 1;
                homeBinding.loading.setVisibility(View.VISIBLE);
                viewModel.getAllRepositoriesResponse("android", page, perPage, sortString);
                observerRepositoryListViewModel();
                alertDialog.cancel();
            }
        });

        updatedDate.setOnClickListener(l -> {
            if (!sortString.equalsIgnoreCase("updated")) {
                sortString = "updated";
                editor.putString("sortBy", "updated");
                editor.commit();
                page = 1;
                homeBinding.loading.setVisibility(View.VISIBLE);
                viewModel.getAllRepositoriesResponse("android", page, perPage, sortString);
                observerRepositoryListViewModel();
                alertDialog.cancel();
            }
        });

        cancelButton.setOnClickListener(l -> alertDialog.cancel());

        alertDialog.show();
    }

    private void loadNewRepository() {
        homeBinding.loading.setVisibility(View.VISIBLE);
        if (isOffline)
            page = 1;
        else
            page++;
        viewModel.getAllRepositoriesResponse("android", page, perPage, sortString);
        observerRepositoryListViewModel();
    }

    private void observerRepositoryListViewModel() {
        viewModel.allRepositories.observe(
                getViewLifecycleOwner(),
                response -> {
                    isOffline = false;
                    if (page * 10 > allRepoList.size())
                        allRepoList.addAll(response.getItems());

                    String jsonSender = new Gson().toJson(allRepoList, new TypeToken<Object>() {
                    }.getType());
                    db.clearRepoList();
                    db.addRepo(jsonSender);

                    if (page == 1)
                        repositoryListAdapter.clearAll();
                    repositoryListAdapter.updateRepositoryList(response.getItems());
                    repositoryListAdapter.notifyDataSetChanged();

                    viewModel.allRepositories = new MutableLiveData<>();
                }
        );
        viewModel.allRepositoriesLoadError.observe(
                getViewLifecycleOwner(), isError -> {
                    if (isError != null) {
                        if (isError) {
                            isOffline = true;
                            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                        viewModel.allRepositoriesLoadError = new MutableLiveData<>();
                    }
                }
        );

        viewModel.noNetwork.observe(
                getViewLifecycleOwner(), isError -> {
                    if (isError != null) {
                        if (isError) {
                            Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                        }
                        viewModel.noNetwork = new MutableLiveData<>();
                    }
                }
        );

        viewModel.loading.observe(
                getViewLifecycleOwner(), loading -> {
                    if (loading != null) {
                        if (!loading) {
                            homeBinding.loading.setVisibility(View.GONE);
                        }
                        viewModel.loading = new MutableLiveData<>();
                    }
                }
        );
    }

    @Override
    public void onStart() {
        super.onStart();
        handler.postDelayed(runnable, 1800000);
    }

    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }
}