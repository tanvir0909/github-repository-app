package com.tanvir.githubrepository.adapter;

import static com.tanvir.githubrepository.utility.Constant.convertStar;
import static com.tanvir.githubrepository.utility.Constant.countDayLeft;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.tanvir.githubrepository.R;
import com.tanvir.githubrepository.databinding.RepositoryItemBinding;
import com.tanvir.githubrepository.model.allRepositories.Item;

import java.util.List;


public class ShowRepositoryListAdapter extends RecyclerView.Adapter<ShowRepositoryListAdapter.ViewHolder> {
    private List<Item> allResultList;
    private Context context;
    public OnClickItem onClickItem;


    public ShowRepositoryListAdapter(List<Item> allResultList, Context context) {
        this.allResultList = allResultList;
        this.context = context;
    }

    public void setOnClickItem(OnClickItem onClickItem) {
        this.onClickItem = onClickItem;
    }

    public void clearAll() {
        this.allResultList.clear();
    }

    public void updateRepositoryList(List<Item> allResultList) {
        this.allResultList.addAll(allResultList);
    }

    public interface OnClickItem {
        void onClickItem(String url);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RepositoryItemBinding itemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.repository_item, parent, false);

        return new ViewHolder(itemBinding.getRoot(), itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Item datum = allResultList.get(position);
        holder.bind(datum);
    }

    @Override
    public int getItemCount() {
        return allResultList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final RepositoryItemBinding itemBinding;

        public ViewHolder(@NonNull View itemView, RepositoryItemBinding itemBinding) {
            super(itemView);
            this.itemBinding = itemBinding;
        }

        public void bind(Item datum) {
            itemBinding.repositoryTitle.setText(datum.getFullName());
            itemBinding.repositoryDescription.setText(datum.getDescription());
            itemBinding.starCount.setText(convertStar(datum.getStargazersCount()));
            itemBinding.updatedDate.setText("Updated " + countDayLeft(datum.getPushedAt()));

            itemBinding.repoList.setOnClickListener(l->{
                onClickItem.onClickItem(datum.getUrl());
            });


            itemBinding.executePendingBindings();

        }
    }
}
