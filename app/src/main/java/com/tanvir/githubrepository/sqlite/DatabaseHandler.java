package com.tanvir.githubrepository.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "repository";
    private static final String TABLE_REPOSITORY_LIST = "repository_list";
    private static final String TABLE_REPOSITORY_DETAILS = "repository_details";
    private static final String TABLE_USER_DETAILS = "user_details";
    private static final String KEY_LIST = "list";
    private static final String KEY_REPO_DETAILS = "repo";
    private static final String KEY_REPO_ID = "repo_id";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_DETAILS = "user";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_REPOSITORY_TABLE = "CREATE TABLE " + TABLE_REPOSITORY_LIST + "(" + KEY_LIST + " TEXT" + ")";
        sqLiteDatabase.execSQL(CREATE_REPOSITORY_TABLE);

        String CREATE_REPO_DETAILS_TABLE = "CREATE TABLE " + TABLE_REPOSITORY_DETAILS + "(" + KEY_REPO_ID + " TEXT PRIMARY KEY," + KEY_REPO_DETAILS + " TEXT" + ")";
        sqLiteDatabase.execSQL(CREATE_REPO_DETAILS_TABLE);

        String CREATE_USER_DETAILS_TABLE = "CREATE TABLE " + TABLE_USER_DETAILS + "(" + KEY_USER_ID + " TEXT PRIMARY KEY," + KEY_USER_DETAILS + " TEXT" + ")";
        sqLiteDatabase.execSQL(CREATE_USER_DETAILS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_REPOSITORY_LIST);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_REPOSITORY_DETAILS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_DETAILS);
        onCreate(sqLiteDatabase);
    }

    //add the new List
    public void addRepo(String list) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_LIST, list);
        db.insert(TABLE_REPOSITORY_LIST, null, values);
        db.close();
    }

    // get all Repository in a list view
    public String getAllRepository() {
        String repoList = "";
        String selectQuery = "SELECT  * FROM " + TABLE_REPOSITORY_LIST;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                repoList = cursor.getString(0);
            } while (cursor.moveToNext());
        }

        return repoList;
    }

    //add the new Repo Details
    public void addRepoDetails(String repoId, String details) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_REPO_ID, repoId);
        values.put(KEY_REPO_DETAILS, details);
        db.insert(TABLE_REPOSITORY_DETAILS, null, values);
        db.close();
    }

    // get the single RepoDetails
    public String getRepoDetails(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_REPOSITORY_DETAILS, new String[]{KEY_REPO_ID, KEY_REPO_DETAILS}, KEY_REPO_ID + "=?",
                new String[]{id}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        if (cursor.getCount() == 1)
            return cursor.getString(1);
        else
            return "";
    }

    // update the single RepoDetails
    public int updateRepoDetails(String repoId, String details) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_REPO_ID, repoId);
        values.put(KEY_REPO_DETAILS, details);

        return db.update(TABLE_REPOSITORY_DETAILS, values, KEY_REPO_ID + " = ?", new String[]{repoId});
    }

    //add the new User Details
    public void addUserDetails(String userId, String details) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userId);
        values.put(KEY_USER_DETAILS, details);
        db.insert(TABLE_USER_DETAILS, null, values);
        db.close();
    }

    // get the single UserDetails
    public String getUserDetails(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_USER_DETAILS, new String[]{KEY_USER_ID, KEY_USER_DETAILS}, KEY_USER_ID + "=?",
                new String[]{id}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();
        if (cursor.getCount() == 1)
            return cursor.getString(1);
        else
            return "";
    }

    // update the single UserDetails
    public int updateUserDetails(String userId, String details) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userId);
        values.put(KEY_USER_DETAILS, details);

        return db.update(TABLE_USER_DETAILS, values, KEY_USER_ID + " = ?", new String[]{userId});
    }

    public void clearRepoList() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_REPOSITORY_LIST);
        db.close();
    }
}
