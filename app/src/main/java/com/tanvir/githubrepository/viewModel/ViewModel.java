package com.tanvir.githubrepository.viewModel;

import androidx.lifecycle.MutableLiveData;

import com.tanvir.githubrepository.api.NetworkService;
import com.tanvir.githubrepository.di.DaggerDependencyComponent;
import com.tanvir.githubrepository.model.allRepositories.GetAllRepositories;
import com.tanvir.githubrepository.model.repoDetails.RepoDetailsResponse;
import com.tanvir.githubrepository.model.userDetails.UserDetailsResponse;

import java.net.UnknownHostException;

import javax.inject.Inject;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ViewModel extends androidx.lifecycle.ViewModel {

    public MutableLiveData<GetAllRepositories> allRepositories = new MutableLiveData<>();
    public MutableLiveData<RepoDetailsResponse> repoDetailsResponse = new MutableLiveData<>();
    public MutableLiveData<UserDetailsResponse> userDetailsResponse = new MutableLiveData<>();

    public MutableLiveData<Boolean> allRepositoriesLoadError = new MutableLiveData<>();
    public MutableLiveData<Boolean> repoDetailsResponseLoadError = new MutableLiveData<>();
    public MutableLiveData<Boolean> userDetailsResponseLoadError = new MutableLiveData<>();

    public MutableLiveData<Boolean> loading = new MutableLiveData<>();
    public MutableLiveData<Boolean> noNetwork = new MutableLiveData<>();

    /**
     * Call network service
     */
    @Inject
    public NetworkService networkService;

    public ViewModel() {
        super();
        DaggerDependencyComponent.create().inject(this);
    }

    private CompositeDisposable disposable = new CompositeDisposable();


    public void getAllRepositoriesResponse(String query, int pageNo, int perPage, String sort) {
        disposable.add(
                networkService.getAllRepositoriesResponse(query, pageNo, perPage, sort)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<GetAllRepositories>() {
                            @Override
                            public void onSuccess(@NonNull GetAllRepositories getAllRepositories) {
                                allRepositories.setValue(getAllRepositories);
                                allRepositoriesLoadError.setValue(false);
                                loading.setValue(false);
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                                if (e instanceof UnknownHostException) {
                                    noNetwork.setValue(true);
                                } else
                                    repoDetailsResponseLoadError.setValue(true);
                                loading.setValue(false);
                            }
                        })
        );

    }

    public void getUserDetailsResponse(String id) {
        disposable.add(
                networkService.getUserDetailsResponse(id)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<UserDetailsResponse>() {
                            @Override
                            public void onSuccess(@NonNull UserDetailsResponse Response) {
                                userDetailsResponse.setValue(Response);
                                userDetailsResponseLoadError.setValue(false);
                                loading.setValue(false);
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                                if (e instanceof UnknownHostException) {
                                    noNetwork.setValue(true);
                                } else
                                    repoDetailsResponseLoadError.setValue(true);
                                loading.setValue(false);
                            }
                        })
        );
    }

    public void getRepoDetailsResponse(String id, String path) {
        disposable.add(
                networkService.getRepoDetailsResponse(id, path)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<RepoDetailsResponse>() {
                            @Override
                            public void onSuccess(@NonNull RepoDetailsResponse Response) {
                                repoDetailsResponse.setValue(Response);
                                repoDetailsResponseLoadError.setValue(false);
                                loading.setValue(false);
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                                if (e instanceof UnknownHostException) {
                                    noNetwork.setValue(true);
                                } else
                                    repoDetailsResponseLoadError.setValue(true);

                                loading.setValue(false);
                            }
                        })
        );
    }


    /**
     * Using clear CompositeDisposable, but can accept new disposable
     */
    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
