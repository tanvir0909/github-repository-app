package com.tanvir.githubrepository.api;

import com.tanvir.githubrepository.model.allRepositories.GetAllRepositories;
import com.tanvir.githubrepository.model.repoDetails.RepoDetailsResponse;
import com.tanvir.githubrepository.model.userDetails.UserDetailsResponse;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @Headers({"Accept: application/json"})
    @GET("search/repositories")
    Single<GetAllRepositories> getAllRepositoriesResponse(
            @Query("q") String query,
            @Query("page") int pageNo,
            @Query("per_page") int perPage,
            @Query("sort") String sort
    );

    @Headers({"Accept: application/json"})
    @GET("users/{id}")
    Single<UserDetailsResponse> getUserDetailsResponse(
            @Path("id") String id
    );

    @Headers({"Accept: application/json"})
    @GET("repos/{id}/{path}")
    Single<RepoDetailsResponse> getRepoDetailsResponse(
            @Path("id") String id,
            @Path("path") String path
    );
}
