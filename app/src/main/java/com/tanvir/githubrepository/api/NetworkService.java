package com.tanvir.githubrepository.api;

import com.tanvir.githubrepository.di.DaggerDependencyComponent;
import com.tanvir.githubrepository.model.allRepositories.GetAllRepositories;
import com.tanvir.githubrepository.model.repoDetails.RepoDetailsResponse;
import com.tanvir.githubrepository.model.userDetails.UserDetailsResponse;

import javax.inject.Inject;
import io.reactivex.rxjava3.core.Single;

public class NetworkService {

    private static NetworkService instance;

    @Inject
    public Api api;

    private NetworkService() {
        DaggerDependencyComponent.create().inject(this);
    }

    public static NetworkService getInstance() {
        if (instance == null) {
            instance = new NetworkService();
        }
        return instance;
    }

    public Single<GetAllRepositories> getAllRepositoriesResponse(String query, int pageNo, int perPage, String sort) {
        return api.getAllRepositoriesResponse(query, pageNo, perPage, sort);
    }

    public Single<UserDetailsResponse> getUserDetailsResponse(String id) {
        return api.getUserDetailsResponse(id);
    }
    public Single<RepoDetailsResponse> getRepoDetailsResponse(String id, String path) {
        return api.getRepoDetailsResponse(id, path);
    }
}
