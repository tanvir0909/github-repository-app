package com.tanvir.githubrepository.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Constant {
    public static String convertStar(int count){
        if (count< 1000){
            return String.valueOf(count);
        } else{
            return String.valueOf(count/1000)+ "K";
        }
    }

    public static String countDayLeft(String date){
        try {
            Date date1;
            Date date2;
            String currentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault()).format(new Date());
            SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            date1 = dates.parse(currentDate);
            date2 = dates.parse(date);
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            String dayDifference;
            if (differenceDates <= 1 )
                dayDifference = "today";
            else
                dayDifference = Long.toString(differenceDates) + " days ago";

            return dayDifference;

        }catch (Exception e){
            return "";
        }
    }

    public static String convertDateTime(String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            Date dateObj = dateFormat.parse(date);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MM-dd-YY HH:mm");
            return simpleDateFormat1.format(dateObj);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }
}
