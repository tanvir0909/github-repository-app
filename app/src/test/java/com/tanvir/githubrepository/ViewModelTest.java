package com.tanvir.githubrepository;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.tanvir.githubrepository.api.NetworkService;
import com.tanvir.githubrepository.model.allRepositories.GetAllRepositories;
import com.tanvir.githubrepository.model.userDetails.UserDetailsResponse;
import com.tanvir.githubrepository.viewModel.ViewModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.internal.schedulers.ExecutorScheduler;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;

public class ViewModelTest {

    private static String q = "android";
    private static int perPage = 10;
    private static int page = 1;
    private static String sortBy = "updated";
    private static String userId = "flutter";

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    @Mock
    NetworkService networkService;

    @InjectMocks
    ViewModel viewModel = new ViewModel();

    private Single<GetAllRepositories> testAllRepositoriesResponse;
    private Single<UserDetailsResponse> testUserDetails;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getRepoListSuccess() {
        GetAllRepositories allRepositories = new GetAllRepositories();
        testAllRepositoriesResponse = Single.just(allRepositories);
        Mockito.when(networkService.getAllRepositoriesResponse(q, page, perPage, sortBy)).thenReturn(testAllRepositoriesResponse);
        viewModel.getAllRepositoriesResponse(q, page, perPage, sortBy);

        Assert.assertEquals(false, viewModel.allRepositoriesLoadError.getValue());
    }


    @Test
    public void getUserDetailsResponse() {
        UserDetailsResponse userDetails = new UserDetailsResponse();
        testUserDetails = Single.just(userDetails);
        Mockito.when(networkService.getUserDetailsResponse(userId)).thenReturn(testUserDetails);
        viewModel.getUserDetailsResponse(userId);

        Assert.assertEquals(false, viewModel.userDetailsResponseLoadError.getValue());
    }


    @Before
    public void setupRxSchedulers() {
        Scheduler immediate = new Scheduler() {
            @Override
            public @NonNull Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(runnable -> runnable.run(), true, true);
            }
        };
        RxJavaPlugins.setInitNewThreadSchedulerHandler(scheduler -> immediate);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> immediate);
    }
}
